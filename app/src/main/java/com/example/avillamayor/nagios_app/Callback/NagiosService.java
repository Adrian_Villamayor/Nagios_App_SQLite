package com.example.avillamayor.nagios_app.Callback;

import com.example.avillamayor.nagios_app.Response.NagiosContent;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NagiosService {

    @GET("/state")
    Call<NagiosContent> getAllNagios();

}

