package com.example.avillamayor.nagios_app.Model;

import com.google.gson.annotations.SerializedName;

public class PING {

    @SerializedName("current_state")
    public String currentState;

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getCurrentState() {
        return currentState;
    }


}
