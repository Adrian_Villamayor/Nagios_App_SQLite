package com.example.avillamayor.nagios_app.Model;

import com.google.gson.annotations.SerializedName;

public class Services {


    @SerializedName("PING")
    public PING pING;

    public void setpING(PING pING) {
        this.pING = pING;
    }

    public PING getpING() {
        return pING;
    }


}
