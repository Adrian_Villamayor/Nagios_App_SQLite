package com.example.avillamayor.nagios_app.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.avillamayor.nagios_app.Helper.dbHelper;

public class DbManager {

    public static final String tName = "host";

    public static final String CN_Name = "name";
    public static final String CN_Server = "server";
    public static final String CN_Current = "current";

    public static String CREATE_TABLE = " create table " + tName + " ("
            + CN_Name + " text primary key,"
            + CN_Server + " text,"
            + CN_Current + " text);";

    private dbHelper helper;
    private SQLiteDatabase db;

    public DbManager(Context context) {
        helper = new dbHelper( context );
        db = helper.getWritableDatabase();

    }

    private ContentValues gContentValues(String nombre, String serv, String curr) {
        ContentValues val = new ContentValues();
        val.put( CN_Name, nombre );
        val.put( CN_Server, serv );
        val.put( CN_Current, curr );
        return val;
    }

    public void insertar(String nombre, String serv, String curr) {
        db.insert( tName, null, gContentValues( nombre, serv, curr ) ); // return -1 Problema

    }

    public void eliminar(String nombre, String serv, String curr) {
        db.delete( tName, CN_Name + "=?", new String[]{nombre} );

    }

    public void actualizar(String nombre, String serv, String curr) {
        db.update( tName, gContentValues( nombre, serv, curr ), CN_Name + "=?", new String[]{nombre} );

    }

    public Cursor listar(){
        String[] colum = new String[]{CN_Name, CN_Server, CN_Current};
        return db.query( tName, colum,null,null, null, null, null);

    }
}
